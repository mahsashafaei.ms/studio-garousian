module.exports = {
  extends: ['@wemake-services/stylelint-config-scss', 'stylelint-config-css-modules', "stylelint-prettier/recommended"],
  rules: {
    'max-line-length': 120,
    'plugin/stylelint-no-indistinguishable-colors': null,
    'color-format/format': null,
    'at-rule-no-unknown': null,
    'scss/at-rule-no-unknown': true,
    'scss/at-mixin-parentheses-space-before': null,
    'scss/operator-no-newline-before': null,
  },
  ignoreFiles: ['**/*.js', '**/*.jsx', '**/*.ts', '**/*.tsx'],
};
