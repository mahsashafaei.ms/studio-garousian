/* eslint-disable */
import css from 'styled-jsx/css';

import blackEot from '../assets/fonts/eot/Vazir-Black-FD.eot';
import blackWoff2 from '../assets/fonts/woff2/Vazir-Black-FD.woff2';
import blackWoff from '../assets/fonts/woff/Vazir-Black-FD.woff';
import blackTtf from '../assets/fonts/ttf/Vazir-Black-FD.ttf';

import boldEot from '../assets/fonts/eot/Vazir-Bold-FD.eot';
import boldWoff2 from '../assets/fonts/woff2/Vazir-Bold-FD.woff2';
import boldWoff from '../assets/fonts/woff/Vazir-Bold-FD.woff';
import boldTtf from '../assets/fonts/ttf/Vazir-Bold-FD.ttf';

import mediumEot from '../assets/fonts/eot/Vazir-Medium-FD.eot';
import mediumWoff2 from '../assets/fonts/woff2/Vazir-Medium-FD.woff2';
import mediumWoff from '../assets/fonts/woff/Vazir-Medium-FD.woff';
import mediumTtf from '../assets/fonts/ttf/Vazir-Medium-FD.ttf';

import lightEot from '../assets/fonts/eot/Vazir-Light-FD.eot';
import lightWoff2 from '../assets/fonts/woff2/Vazir-Light-FD.woff2';
import lightWoff from '../assets/fonts/woff/Vazir-Light-FD.woff';
import lightTtf from '../assets/fonts/ttf/Vazir-Light-FD.ttf';

import regularEot from '../assets/fonts/eot/Vazir-Regular-FD.eot';
import regularWoff2 from '../assets/fonts/woff2/Vazir-Regular-FD.woff2';
import regularWoff from '../assets/fonts/woff/Vazir-Regular-FD.woff';
import regularTtf from '../assets/fonts/ttf/Vazir-Regular-FD.ttf';

import thinEot from '../assets/fonts/eot/Vazir-Thin-FD.eot';
import thinWoff2 from '../assets/fonts/woff2/Vazir-Thin-FD.woff2';
import thinWoff from '../assets/fonts/woff/Vazir-Thin-FD.woff';
import thinTtf from '../assets/fonts/ttf/Vazir-Thin-FD.ttf';

export const globalStyles = css.global`
  @font-face {
    font-family: IRANSans;
    font-style: normal;
    font-weight: 900;
    src: url('${blackEot}');
    src: url('${blackEot + '?#iefix'}') format('embedded-opentype'), /* IE6-8 */ url('${blackWoff2}') format('woff2'),
      /* FF39+,Chrome36+, Opera24+*/ url('${blackWoff}') format('woff'),
      /* FF3.6+, IE9, Chrome6+, Saf5.1+*/ url('${blackTtf}') format('truetype');
  }
  @font-face {
    font-family: IRANSans;
    font-style: normal;
    font-weight: bold;
    src: url('${boldEot}');
    src: url('${boldEot + '?#iefix'}') format('embedded-opentype'), /* IE6-8 */ url('${boldWoff2}') format('woff2'),
      /* FF39+,Chrome36+, Opera24+*/ url('${boldWoff}') format('woff'),
      /* FF3.6+, IE9, Chrome6+, Saf5.1+*/ url('${boldTtf}') format('truetype');
  }
  @font-face {
    font-family: IRANSans;
    font-style: normal;
    font-weight: 500;
    src: url('${mediumEot}');
    src: url('${mediumEot + '?#iefix'}') format('embedded-opentype'), /* IE6-8 */ url('${mediumWoff2}') format('woff2'),
      /* FF39+,Chrome36+, Opera24+*/ url('${mediumWoff}') format('woff'),
      /* FF3.6+, IE9, Chrome6+, Saf5.1+*/ url('${mediumTtf}') format('truetype');
  }
  @font-face {
    font-family: IRANSans;
    font-style: normal;
    font-weight: 300;
    src: url('${lightEot}');
    src: url('${lightEot + '?#iefix'}') format('embedded-opentype'), /* IE6-8 */ url('${lightWoff2}') format('woff2'),
      /* FF39+,Chrome36+, Opera24+*/ url('${lightWoff}') format('woff'),
      /* FF3.6+, IE9, Chrome6+, Saf5.1+*/ url('${lightTtf}') format('truetype');
  }
  @font-face {
    font-family: IRANSans;
    font-style: normal;
    font-weight: normal;
    src: url('${regularEot}');
    src: url('${regularEot + '?#iefix'}') format('embedded-opentype'),
      /* IE6-8 */ url('${regularWoff2}') format('woff2'),
      /* FF39+,Chrome36+, Opera24+*/ url('${regularWoff}') format('woff'),
      /* FF3.6+, IE9, Chrome6+, Saf5.1+*/ url('${regularTtf}') format('truetype');
  }
  @font-face {
    font-family: IRANSans;
    font-style: normal;
    font-weight: 100;
    src: url('${thinEot}');
    src: url('${thinEot + '?#iefix'}') format('embedded-opentype'), /* IE6-8 */ url('${thinWoff2}') format('woff2'),
      /* FF39+,Chrome36+, Opera24+*/ url('${thinWoff}') format('woff'),
      /* FF3.6+, IE9, Chrome6+, Saf5.1+*/ url('${thinTtf}') format('truetype');
  }
  blockquote,
  dl,
  dd,
  h1,
  h2,
  h3,
  h4,
  h5,
  h6,
  hr,
  figure,
  p,
  pre {
    margin: 0;
  }

  button {
    background-color: transparent;
    background-image: none;
    border: none;
  }
  fieldset {
    margin: 0;
    padding: 0;
  }

  ol,
  ul {
    list-style: none;
    margin: 0;
    padding: 0;
  }

  html {
    font-family: 'IRANSans';
    line-height: 1.5;
    overflow-x: hidden;
  }

  body {
    font-family: 'IRANSans';
    line-height: inherit;
    direction: rtl;
    overflow-x: hidden;
    margin: 0;
  }

  ::before,
  ::after {
    box-sizing: border-box;
    border-width: 0;
    border-style: solid;
    border-color: theme('borderColor.DEFAULT', currentColor);
  }

  hr {
    border-top-width: 0.0625rem;
  }

  textarea {
    resize: vertical;
  }

  input::placeholder,
  textarea::placeholder {
    opacity: 1;
    color: theme('colors.gray.400', #a1a1aa);
  }

  button,
  [role='button'] {
    cursor: pointer;
  }

  table {
    border-collapse: collapse;
  }

  h1,
  h2,
  h3,
  h4,
  h5,
  h6 {
    font-size: inherit;
    font-weight: inherit;
  }

  a {
    color: inherit;
    text-decoration: inherit;
  }

  button,
  input,
  optgroup,
  select,
  textarea {
    padding: 0;
    line-height: inherit;
    color: inherit;
  }

  pre,
  code,
  kbd,
  samp {
    font-family: 'IRANSans';
  }

  img,
  svg,
  video,
  canvas,
  audio,
  iframe,
  embed,
  object {
    vertical-align: middle;
  }

  img,
  video {
    max-width: 100%;
    height: auto;
  }

  .drop-shadow {
    --tw-drop-shadow: drop-shadow(0 1px 2px rgba(0, 0, 0, 0.1)) drop-shadow(0 1px 1px rgba(0, 0, 0, 0.06));
  }

  div#__next {
    overflow-y: hidden;
  }
`;
