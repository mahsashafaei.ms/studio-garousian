/* eslint-disable */
const withImages = require('next-images');
const withFonts = require('next-fonts');
const basePath = process.env.BASE_PATH || '';

module.exports = withImages(
  withFonts({
    basePath: basePath || '',
    assetPrefix: basePath || '',
    publicRuntimeConfig: {
      basePath: basePath || '',
    },
    webpack(config, options) {
      config.module.rules.push({
        test: /\.(graphql|gql)$/,
        exclude: /node_modules/,
        loader: 'graphql-tag/loader',
      });
      return config;
    },
  }),
);
