module.exports = {
  parser: '@typescript-eslint/parser',
  plugins: ['react', '@typescript-eslint'],
  env: {
    browser: true,
    es6: true,
    node: true,
  },
  extends: [
    'eslint:recommended',
    'airbnb-typescript',
    'airbnb/hooks',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:@typescript-eslint/recommended-requiring-type-checking',
    'plugin:react/recommended',
    'plugin:prettier/recommended',
    'prettier',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
    project: './tsconfig.json',
  },
  rules: {
    'react/prop-types': 0,
    // 'max-len': [2, 120, 2, { ignoreUrls: true, ignorePattern: '\\s+it\\(' }],
    'object-curly-newline': 0,
    'import/prefer-default-export': 0,
    'react/jsx-one-expression-per-line': 0,
    '@typescript-eslint/indent': 0,
    'no-plusplus': 0,
    'react/jsx-wrap-multilines': ['error', { declaration: false, assignment: false }],
    '@typescript-eslint/camelcase': 0,
    'no-unused-vars': [1, { varsIgnorePattern: '^_' }],
    '@typescript-eslint/no-unused-vars': [1, { varsIgnorePattern: '^_' }],
    '@typescript-eslint/no-floating-promises': 0,
    'no-extra-boolean-cast': 0,
    '@typescript-eslint/no-unsafe-member-access': 0, // should be remove
    '@typescript-eslint/naming-convention': [
      'error',
      {
        selector: 'variable',
        format: ['camelCase', 'PascalCase', 'UPPER_CASE'],
        leadingUnderscore: 'allow',
      },
      {
        selector: 'function',
        format: ['camelCase', 'PascalCase'],
      },
      {
        selector: 'typeLike',
        format: ['PascalCase'],
      },
    ],
    'react/react-in-jsx-scope': 'off',
    'jsx-a11y/anchor-is-valid': [
      'error',
      {
        components: ['Link'],
        specialLink: ['hrefLeft', 'hrefRight'],
        aspects: ['invalidHref', 'preferButton'],
      },
    ],
    'import/no-extraneous-dependencies': ['error', { devDependencies: true }],
    'react/jsx-props-no-spreading': 'off',
    'react/no-danger': 0,
  },
  overrides: [
    {
      files: ['**/*.js', '**/*.jsx'],
      rules: {
        '@typescript-eslint/no-var-requires': 0,
        '@typescript-eslint/explicit-function-return-type': 0,
      },
    },
  ],
};
