/* eslint-disable */
import css from 'styled-jsx/css';

export default css`
  .background {
    background-color: #606060;
    padding: 3.125rem 1.25rem;
  }
  .videoDiv {
    padding-top: 1.875rem;
    display: flex;
    align-items: center;
    flex-direction: row;
    position: relative;
  }

  .playBtn {
    width: 100%;
    height: auto;
    z-index: 10;
    position: absolute;
    display: flex;
    align-items: center;
    justify-content: space-evenly;
  }

  .videoIntroduction {
    object-fit: cover;
    width: 100%;
    opacity: 50%;
  }

  .descriptionDiv {
    color: #fff;
    padding: 0 0.875rem;
  }
  .descriptionDiv h2 {
    font-size: 1.3125rem;
    font-weight: 500;
    text-align: center;
  }
  .descriptionDiv ul {
    padding-top: 2.5rem;
  }
  .descriptionDiv ul li {
    display: flex;
    align-items: baseline;
    justify-content: space-between;
  }
  .descriptionDiv ul li:nth-child(1) {
    margin-bottom: 1.25rem;
  }
  .descriptionDiv ul li img {
    padding-left: 1rem;
  }
  .descriptionDiv ul li p {
    font-size: 0.875rem;
    font-weight: 300;
    text-align: justify;
  }
  .play {
    width: 10.5%;
  }

  @media (min-width: 900px) {
    .background {
      display: flex;
      justify-content: space-between;
      align-items: center;
      flex-direction: row-reverse;
      padding: 3.5rem 2.75rem;
    }
    .descriptionDiv {
      color: #fff;
      width: 76%;
      padding-right: 6.625rem;
    }
    .descriptionDiv h2 {
      text-align: right;
      font-size: 1.375rem;
    }
    .descriptionDiv ul {
      padding-right: 0.5rem;
      padding-top: 3.125rem;
    }
    .descriptionDiv ul li p {
      font-size: 1.125rem;
    }
    .videoDiv {
      padding-top: 1.375rem;
      padding-bottom: 1.375rem;
      width: 48%;
    }
    .play {
      width: auto;
    }
  }
  @media (min-width: 1100px) {
    .background {
      padding: 5.5rem 8.75rem;
    }
  }
`;
