import React, { FC, ReactElement } from 'react';
import styles from './VideoIntroduction.style';
import circle from '../../../assets/svg/circle.svg';
import playBtn from '../../../assets/svg/playBtn.svg';
import videoIntroduction from '../../../assets/svg/videoIntroduction.svg';

const VideoIntroduction: FC = (): ReactElement => {
  return (
    <div className="background">
      <div className="descriptionDiv">
        <h2>ویدئو معرفی ما را تماشا کنید</h2>
        <ul>
          <li>
            <img src={circle} alt="circle" />
            <p>
              این یک پاراگراف است. برای افزودن یا ویرایش متن خود اینجا را کلیک کنید. این متن باید برای گفتن یک داستان
              مورد استفاده قرار گیرد و کاربران اطلاعات بیشتری بدست آورند.
            </p>
          </li>
          <li>
            <img src={circle} alt="circle" />
            <p>
              این یک پاراگراف است. برای افزودن یا ویرایش متن خود اینجا را کلیک کنید. این متن باید برای گفتن یک داستان
              مورد استفاده قرار گیرد و کاربران اطلاعات بیشتری بدست آورند.
            </p>
          </li>
        </ul>
      </div>
      <div className="videoDiv">
        <button type="submit" className="playBtn">
          <img src={playBtn} alt="play button" className="play" />
        </button>
        <img className="videoIntroduction" src={videoIntroduction} alt="Video Introduction" />
      </div>
      <style jsx>{styles}</style>
    </div>
  );
};
export default VideoIntroduction;
