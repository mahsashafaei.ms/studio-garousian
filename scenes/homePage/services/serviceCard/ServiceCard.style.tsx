/* eslint-disable */
import css from 'styled-jsx/css';

export default css`
  .serviceInfo {
    border-radius: 0.5rem;
    background-color: #fff;
    padding: 1.6875rem 1.75rem;
    box-shadow: 0 0.1875rem 2.875rem rgba(0, 0, 0, 10%);
    width: 76%;
    margin: auto;
    z-index: 10;
  }
  .serviceCard {
    display: flex;
    flex-direction: column;
    justify-content: end;
    align-items: center;
    z-index: 10;
  }
  .serviceCard img {
    margin-bottom: -9%;
  }
  .serviceCard h3 {
    margin-bottom: 1rem;
    font-size: 1.1875rem;
    font-weight: 500;
    color: #333333;
  }
  .serviceCard p {
    font-size: 0.875rem;
    line-height: 1.1875rem;
    font-weight: 300;
    color: #62758d;
  }
  .serviceButtons {
    margin: 1.25rem 0 0.5rem 0;
    display: flex;
    align-items: center;
    justify-content: space-evenly;
  }

  .buttonDiv {
    width: 45%;
  }
  @media (min-width: 600px) {
    .serviceInfo {
      width: 60%;
    }
  }
  @media (min-width: 700px) {
    .serviceInfo {
      width: 50%;
    }
  }
  @media (min-width: 767px) {
    .serviceInfo {
      width: 45%;
    }
  }
  @media (min-width: 900px) {
    .serviceInfo {
      width: 38%;
    }
  }
  @media (min-width: 1000px) {
    .serviceInfo {
      padding: 1.625rem 1.5rem;
    }
    .serviceInfo {
      width: 75%;
    }
  }
  @media (min-width: 1300px) {
    .serviceInfo {
      padding: 2.625rem 2.5rem;
    }
  }
  @media (min-width: 1400px) {
    .serviceInfo {
      width: 80%;
    }
  }
  @media (min-width: 900px) {
    .serviceCard img {
      margin-bottom: -3.25rem;
    }

    .serviceCard h3 {
      margin-bottom: 0.5rem;
      font-size: 1.25rem;
    }
    .serviceCard p {
      font-size: 1rem;
      line-height: normal;
    }
    .serviceButtons {
      margin-top: 2.5rem;
    }
  }
`;
