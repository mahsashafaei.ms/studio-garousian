import React, { FC, ReactElement } from 'react';
import Button from '../../../../components/button/Button';
import styles from './ServiceCard.style';

interface ButtonProps {
  buttonType: 'purpleNeutralFilled' | 'greenNeutralFilled' | 'orangeNeutralFilled';
  buttonTitle: string;
}

interface Props {
  imgSrc: string;
  title: string;
  description: string;
  buttons: ButtonProps[];
}
const ServiceCard: FC<Props> = ({ imgSrc, title, description, buttons }): ReactElement => {
  return (
    <div className="serviceCard">
      <img src={imgSrc} alt="services" />
      <div className="serviceInfo">
        <h3>{title}</h3>
        <p>{description}</p>
        <div className="serviceButtons">
          {buttons.map((button) => (
            <div className="buttonDiv" key={button.buttonTitle}>
              <Button type={button.buttonType}>{button.buttonTitle}</Button>
            </div>
          ))}
        </div>
      </div>
      <style jsx>{styles}</style>
    </div>
  );
};
export default ServiceCard;
