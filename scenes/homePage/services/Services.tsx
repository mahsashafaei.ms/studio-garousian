import React, { FC, ReactElement } from 'react';
import ServiceCard from './serviceCard/ServiceCard';
import webService from '../../../assets/svg/webService.svg';
import financialService from '../../../assets/svg/financialService.svg';
import designService from '../../../assets/svg/designService.svg';
import styles from './Services.style';

const Services: FC = (): ReactElement => {
  return (
    <div className="services">
      <h2>ما خدمات زیادی به مشتریان بزرگمان ارائه می‌دهیم</h2>
      <p>
        از این زیر‌عنوان برای اطلاع رسانی به کاربر و آشنایی بیشتر با محصول و خدمات خود استفاده می‌کنیم. با کلیک بر روی
        این قسمت می‌توانید متن خود را ویرایش کنید
      </p>
      <div className="servicesCards">
        <ServiceCard
          imgSrc={webService}
          title="تجزیه و تحلیل وب"
          description="اینجا می‌توانید کمی در مورد خودتون صحبت و توضیح دهید که چه خدماتی ارائه می‌دهید"
          buttons={[
            { buttonType: 'purpleNeutralFilled', buttonTitle: 'Marketing' },
            { buttonType: 'purpleNeutralFilled', buttonTitle: 'SEO' },
          ]}
        />
        <div className="gap" />
        <ServiceCard
          imgSrc={financialService}
          title="خدمات مالی"
          description="اینجا می‌توانید کمی در مورد خودتون صحبت و توضیح دهید که چه خدماتی ارائه می‌دهید"
          buttons={[
            { buttonType: 'greenNeutralFilled', buttonTitle: 'Saftey Pay' },
            { buttonType: 'greenNeutralFilled', buttonTitle: 'Best Invest' },
          ]}
        />
        <div className="gap" />
        <ServiceCard
          imgSrc={designService}
          title="طراحی جدید"
          description="اینجا می‌توانید کمی در مورد خودتون صحبت و توضیح دهید که چه خدماتی ارائه می‌دهید"
          buttons={[
            { buttonType: 'orangeNeutralFilled', buttonTitle: 'IT Solution' },
            { buttonType: 'orangeNeutralFilled', buttonTitle: 'Web Design' },
          ]}
        />
      </div>
      <style jsx>{styles}</style>
    </div>
  );
};
export default Services;
