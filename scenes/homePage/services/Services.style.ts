/* eslint-disable */
import css from 'styled-jsx/css';

export default css`
  .services {
    margin: 3.125rem 1.25rem;
    text-align: center;
  }
  .servicesCards {
    margin-top: 2.8rem;
  }

  h2 {
    margin-bottom: 1rem;
    font-weight: 500;
    font-size: 1.3125rem;
    color: #333333;
  }
  p {
    font-weight: 300;
    font-size: 1em;
    color: #808080;
    margin: auto;
    line-height: 1.3125rem;
  }
  .gap {
    height: 1.875rem;
  }
  @media (min-width: 600px) {
    p {
      width: 80%;
    }
  }
  @media (min-width: 767px) {
    p {
      width: 70%;
    }
  }
  @media (min-width: 900px) {
    p {
      width: 80%;
    }
  }
  @media (min-width: 1000px) {
    p {
      width: 70%;
    }
  }
  @media (min-width: 1200px) {
    p {
      width: 60%;
    }
  }
  @media (min-width: 1300px) {
    p {
      width: 53%;
    }
  }
  @media (min-width: 1400px) {
    p {
      width: 40%;
    }
  }
  @media (min-width: 1000px) {
    .services {
      margin: 6.875rem 4.3125rem;
    }
    h2 {
      font-size: 1.75rem;
    }
    p {
      font-weight: normal;
      font-size: 1.125rem;
    }
    .servicesCards {
      display: flex;
      align-items: center;
      justify-content: space-between;
      margin-top: 3rem;
    }
    .gap {
      width: 1.875rem;
    }
  }
`;
