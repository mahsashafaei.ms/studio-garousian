/* eslint-disable */
import css from 'styled-jsx/css';

export default css`
  .backgroundItems {
    padding: 3.125rem 1.375rem;
    margin: auto;
    text-align: center;
    overflow-y: hidden;
  }
  .texts h1 {
    font-size: 1.5625rem;
    line-height: 2.5625rem;
    margin-bottom: 1rem;
    font-weight: 500;
  }
  .texts .description {
    width: auto;
    line-height: 1.3125rem;
    font-size: 1rem;
    font-weight: normal;
  }
  .texts {
    margin-top: 3rem;
    color: #fff;
  }
  .buttons {
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    margin-top: 3rem;
  }

  .logo {
    width: 31%;
  }
  .gap {
    width: 0.625rem;
  }
  @media (min-width: 600px) {
    .texts .description {
      width: 82%;
      margin: auto;
    }
  }
  @media (min-width: 700px) {
    .texts .description {
      width: 70%;
    }
  }
  @media (min-width: 1220px) {
    .logo {
      width: auto;
    }
    .backgroundItems {
      padding-top: 9.375rem;
      padding-bottom: 7rem;
    }
    .texts {
      margin-top: 5.375rem;
    }
    .texts p {
      font-size: 2rem;
      line-height: 3.375rem;
      margin-bottom: 0.25rem;
    }
    .texts .description {
      line-height: normal;
      font-size: 1.125rem;
    }
    .buttons {
      margin-top: 4rem;
    }
    .gap {
      width: 1.375rem;
    }
  }
`;
