import React, { FC, ReactElement } from 'react';
import Button from '../../../components/button/Button';
import Background from '../../../components/background/Background';
import styles from './AboutUs.style';
import Logo from '../../../assets/svg/logo.svg';
import aboutUsBack from '../../../assets/svg/aboutUsBack.svg';

const AboutUs: FC = (): ReactElement => {
  return (
    <Background src={aboutUsBack}>
      <div className="backgroundItems">
        <img className="logo" src={Logo} alt="Logo" />
        <div className="texts">
          <h1>به راحتی خدمات مورد نیاز خودت رو پیدا کن!</h1>
          <p className="description">
            اینجا می‌توانید کمی در مورد خودتون صحبت و توضیح دهید که محصول شما چیست و چه کارهایی انجام می‌ده
          </p>
        </div>
        <div className="buttons">
          <Button type="primaryFilled">مشاوره می خواهم</Button>
          <div className="gap" />
          <Button type="primary">فرم تماس با ما</Button>
        </div>
      </div>

      <style jsx>{styles}</style>
    </Background>
  );
};
export default AboutUs;
