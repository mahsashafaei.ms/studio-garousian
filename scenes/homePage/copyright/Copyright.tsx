import React, { FC, ReactElement } from 'react';
import styles from './Copyright.style';

const Copyright: FC = (): ReactElement => {
  return (
    <div className="copyright">
      <p>©2019 . Company . All rights reserved</p>
      <style jsx>{styles}</style>
    </div>
  );
};
export default Copyright;
