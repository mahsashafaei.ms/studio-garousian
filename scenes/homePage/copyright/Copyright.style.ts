/* eslint-disable */
import css from 'styled-jsx/css';

export default css`
  .copyright {
    background-color: #333333;
    padding: 0.5rem 0;
  }
  .copyright p {
    font-size: 0.75rem;
    font-weight: 100;
    color: #979797;
    text-align: center;
    line-height: 0.875rem;
    direction: ltr;
  }
  @media (min-width: 767px) {
    .copyright {
      padding: 1.5rem 0;
    }
    .copyright p {
      font-size: 1.125rem;
      line-height: 1.25rem;
    }
  }
`;
