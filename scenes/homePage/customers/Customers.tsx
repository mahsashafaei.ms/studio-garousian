import React, { FC, ReactElement } from 'react';
import CustomerCard from './customerCard/CustomerCard';
import CustomersLogo from './customersLogo/CustomersLogo';
import customer_1 from '../../../assets/svg/customer_1.svg';
import customer_2 from '../../../assets/svg/customer_2.svg';
import customerLogo_1 from '../../../assets/svg/customerLogo_1.svg';
import customerLogo_2 from '../../../assets/svg/customerLogo_2.svg';
import customerLogo_3 from '../../../assets/svg/customerLogo_3.svg';
import customerLogo_4 from '../../../assets/svg/customerLogo_4.svg';
import customerLogo_5 from '../../../assets/svg/customerLogo_5.svg';
import styles from './Customers.style';

const Customers: FC = (): ReactElement => {
  return (
    <div className="customers">
      <h1>مشتریان ما در موردمان چه می‌گویند</h1>
      <p>
        از این زیر‌عنوان برای اطلاع رسانی به کاربر و آشنایی بیشتر با محصول و خدمات خود استفاده می‌کنیم. با کلیک بر روی
        این قسمت می‌توانید متن خود را ویرایش کنید
      </p>
      <div className="customersDiv">
        <div className="customerCard">
          <CustomerCard
            imgSrc={customer_1}
            title="سهراب یزدانی"
            description="این یک پاراگراف است. برای افزودن یا ویرایش متن خود اینجا را کلیک کنید. این متن باید  برای گفتن یک داستان مورد استفاده قرار گیرد و کاربران اطلاعات بیشتری بدست آورند."
            job="عنوان شغل، اسم شرکت"
          />
        </div>
        <div className="customerCard">
          <CustomerCard
            imgSrc={customer_2}
            title="علی جوانمردی"
            description="این یک پاراگراف است. برای افزودن یا ویرایش متن خود اینجا را کلیک کنید. این متن باید  برای گفتن یک داستان مورد استفاده قرار گیرد و کاربران اطلاعات بیشتری بدست آورند."
            job="عنوان شغل، اسم شرکت"
          />
        </div>
      </div>
      <div className="customersLogo">
        <CustomersLogo logos={[customerLogo_1, customerLogo_2, customerLogo_3, customerLogo_4, customerLogo_5]} />
      </div>
      <style jsx>{styles}</style>
    </div>
  );
};
export default Customers;
