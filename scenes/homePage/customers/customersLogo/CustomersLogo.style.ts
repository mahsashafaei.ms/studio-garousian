/* eslint-disable */
import css from 'styled-jsx/css';

export default css`
  .logos {
    display: flex;
    align-items: center;
    justify-content: center;
    height: 3.25rem;
  }
  .logos img {
    margin: 0 1rem;
    width: 11%;
    height: 85%;
  }
  @media (min-width: 767px) {
    .logos img {
      margin: 0 2.1875rem;
      width: auto;
      height: auto;
    }

    .logos {
      height: auto;
    }
  }
`;
