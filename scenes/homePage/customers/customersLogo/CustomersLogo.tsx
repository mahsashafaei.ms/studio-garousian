import React, { FC, ReactElement } from 'react';
import styles from './CustomersLogo.style';

interface Props {
  logos: string[];
}
const CustomerLogos: FC<Props> = ({ logos }): ReactElement => {
  return (
    <div className="logos">
      {logos.map((logo) => (
        <img key={logo} src={logo} alt="customer's logo" />
      ))}
      <style jsx>{styles}</style>
    </div>
  );
};
export default CustomerLogos;
