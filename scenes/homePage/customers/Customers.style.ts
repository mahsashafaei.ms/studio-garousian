/* eslint-disable */
import css from 'styled-jsx/css';

export default css`
  .customers {
    background-color: #f2f2f2;
    padding: 3.125rem 1rem;
  }
  .customers h1 {
    font-size: 1.3125rem;
    font-weight: 500;
    text-align: center;
    color: #333333;
    margin-bottom: 0.625rem;
    padding: 0 0.875rem;
  }
  .customers p {
    line-height: normal;
    margin: auto;
    color: #808080;
    font-size: 1rem;
    font-weight: 300;
    text-align: center;
    padding: 0 0.875rem;
  }
  .customersDiv {
    margin-top: 1.875rem;
  }

  .customersLogo {
    padding-top: 1.25rem;
  }
  .customerCard {
    margin-bottom: 1.875rem;
  }
  @media (min-width: 480px) {
    .customers p {
      width: 71%;
    }
  }

  @media (min-width: 767px) {
    .customers {
      padding: 4.25rem 1.875rem;
    }
    .customers h1 {
      font-size: 1.75rem;
      margin-bottom: 0.5rem;
      padding: 0;
    }
    .customers p {
      line-height: normal;
      padding: 0;
    }
    .customersDiv {
      display: flex;
      justify-content: space-between;
      align-items: center;
      flex-direction: row;
      margin-top: 4.375rem;
    }
    .customerCard {
      width: 48%;
      margin-bottom: 0;
    }

    .customersLogo {
      margin-top: 5.9375rem;
      padding-top: 0;
    }
  }
  @media (min-width: 1000px) {
    .customers p {
      width: 55%;
    }
  }
  @media (min-width: 1200px) {
    .customers p {
      width: 45%;
    }
  }
  @media (min-width: 1300px) {
    .customers {
      padding: 6.25rem 11.875rem;
    }
  }
`;
