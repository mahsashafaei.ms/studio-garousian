import React, { FC, ReactElement } from 'react';
import styles from './CustomerCard.style';

interface Props {
  imgSrc: string;
  title: string;
  description: string;
  job: string;
}

const Customers: FC<Props> = ({ imgSrc, title, description, job }): ReactElement => {
  return (
    <div className="card">
      <img src={imgSrc} alt="profile" />
      <div className="information">
        <span className="mobTitle">{title}</span>
        <span className="mobJob">{job}</span>
        <p>{description}</p>
        <span className="title">{title}</span>
        <span className="job">{job}</span>
      </div>
      <style jsx>{styles}</style>
    </div>
  );
};
export default Customers;
