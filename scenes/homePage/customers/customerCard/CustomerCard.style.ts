/* eslint-disable */
import css from 'styled-jsx/css';

export default css`
  .card {
    padding: 2.5rem 1.3125rem;
    box-shadow: 0 0.1875rem 2.0625rem rgba(0, 0, 0, 7%);
    border-radius: 0.625rem;
    background: #fff;
    text-align: center;
  }
  .information {
    color: #404040;
    margin-top: 2.5rem;
  }
  .information p {
    font-size: 1;
    font-weight: 300;
    text-align: justify;
    line-height: 1.3125rem;
  }
  .information span {
    display: block;
  }
  .information .title {
    display: none;
  }

  .information .job {
    display: none;
  }
  .information .mobTitle {
    font-size: 1.25rem;
    font-weight: 500;
    margin-bottom: 0.5rem;
    line-height: 1.6875rem;
  }
  .information .mobJob {
    font-weight: 500;
    font-size: 1rem;
    margin-bottom: 1.875rem;
    line-height: 1.3125rem;
    color: #808080;
  }
  @media (min-width: 1000px) {
    .card {
      display: flex;
      justify-content: space-between;
      flex-direction: row;
      align-items: flex-start;
      padding: 4.25rem 3.125rem;
    }
    .information {
      margin-right: 2.5rem;
      margin-top: 0rem;
    }
    .information .mobTitle {
      display: none;
    }
    .information .mobJob {
      display: none;
    }
    .information .title {
      display: block;
      font-size: 1.25rem;
      font-weight: 500;
      margin-bottom: 0.5rem;
      text-align: right;
    }
    .information .job {
      display: block;
      font-size: 0.9375rem;
      font-weight: 300;
      text-align: right;
    }
    .information p {
      font-size: 1.25rem;
      font-weight: 100;
      margin-bottom: 2.5rem;
      line-height: 1.5625rem;
    }
  }
`;
