import React, { FC, ReactElement } from 'react';
import Button from '../../../components/button/Button';
import styles from './Cooperation.style';
import cooperation from '../../../assets/svg/cooperation.svg';

const Cooperation: FC = (): ReactElement => {
  return (
    <div className="backgroundDiv">
      <div className="itemsDiv">
        <div className="backgroundItems">
          <div className="texts">
            <h1>اگر علاقه‌مند به همکاری با ما هستی</h1>
            <p className="description">
              از این زیر‌عنوان برای اطلاع رسانی به کاربر و آشنایی بیشتر با محصول و خدمات خود استفاده می‌کنیم. با کلیک بر
              روی این قسمت می‌توانید متن خود را ویرایش کنید
            </p>
          </div>
          <div className="mobileButtons">
            <a className="btn primaryFilledBtn">ارسال رزومه</a>
            <span className="gap" />
            <a className="btn ">فرم تماس با ما</a>
          </div>

          <div className="buttons">
            <Button type="primaryFilled">ارسال رزومه</Button>
            <span className="gap" />
            <Button type="primary">فرم تماس با ما</Button>
          </div>
        </div>
      </div>
      <img src={cooperation} alt="background" className="backgroundImg" />
      <style jsx>{styles}</style>
    </div>
  );
};
export default Cooperation;
