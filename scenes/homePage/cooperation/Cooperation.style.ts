/* eslint-disable */
import css from 'styled-jsx/css';

export default css`
  .backgroundDiv {
    width: 100%;
    position: relative;
    overflow-y: hidden;
    overflow-x: hidden;
    display: flex;
    align-items: center;
  }
  .itemsDiv {
    width: 100%;
    display: flex;
    align-items: center;
    flex-direction: row;
    z-index: 10;
    position: absolute;
  }
  .backgroundImg {
    object-fit: cover;
    filter: brightness(35%);
    height: 26.25rem;
  }
  .backgroundItems {
    padding: 5.5rem 1.5625rem;
    margin: auto;
    text-align: center;
    width: 100%;
  }
  .texts h1 {
    font-size: 1.3125rem;
    line-height: 2.375rem;
    margin-bottom: 1rem;
    font-weight: 500;
  }
  .texts .description {
    line-height: 1.3125rem;
    font-size: 1rem;
    font-weight: 300;
    margin: auto;
  }
  .texts {
    color: #fff;
  }
  .mobileButtons {
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    margin-top: 2.5rem;
  }
  .buttons {
    display: none;
  }
  .gap {
    width: 0.625rem;
  }
  .btn {
    border-radius: 0.25rem;
    font-weight: 500;
    font-size: 1rem;
    padding: 0.625rem 1.625rem;
    border: 1px solid #fff;
    color: #fff;
  }
  .primaryFilledBtn {
    background: #5274f9;
    border: 1px solid #5274f9;
    padding: 0.625rem 2.25rem;
  }

  @media (max-width: 320px) {
    .btn {
      padding: 0.625rem 1.3rem;
    }
    .primaryFilledBtn {
      padding: 0.625rem 1.5rem;
    }
  }
  @media (min-width: 480px) {
    .texts .description {
      width: 73%;
    }
  }
  @media (min-width: 700px) {
    .texts .description {
      width: 55%;
    }
  }
  @media (min-width: 700px) {
    .texts .description {
      width: 46%;
    }
  }

  @media (min-width: 1000px) {
    .backgroundImg {
      height: auto;
    }
    .backgroundItems {
      padding: 8.625rem 0;
      margin: auto;
      text-align: center;
    }
    .texts h1 {
      font-size: 2rem;
      line-height: 3.375rem;
      margin-bottom: 0.5rem;
      font-weight: 500;
    }
    .texts .description {
      line-height: normal;
      font-size: 1.125rem;
      font-weight: normal;
    }
    .buttons {
      margin-top: 4rem;
    }
    .gap {
      width: 1.375rem;
    }
    .mobileButtons {
      display: none;
    }
    .buttons {
      display: flex;
      flex-direction: row;
      align-items: center;
      justify-content: center;
      margin-top: 2.5rem;
    }
  }
`;
