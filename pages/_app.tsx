import { ReactElement } from 'react';
import type { AppProps } from 'next/app';
import { globalStyles } from '../styles/globals';

export default function App({ Component, pageProps }: AppProps): ReactElement {
  return (
    <>
      <Component {...pageProps} />
      <style jsx>{globalStyles}</style>
    </>
  );
}
