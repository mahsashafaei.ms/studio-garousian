import Head from 'next/head';
import { NextPage } from 'next';
import AboutUs from '../scenes/homePage/aboutUs/AboutUs';
import Services from '../scenes/homePage/services/Services';
import VideoIntroduction from '../scenes/homePage/videoIntroduction/VideoIntroduction';
import Customers from '../scenes/homePage/customers/Customers';
import Cooperation from '../scenes/homePage/cooperation/Cooperation';
import Copyright from '../scenes/homePage/copyright/Copyright';

const HomePage: NextPage = () => {
  return (
    <>
      <Head>
        <title>Lead Generation Company</title>
        <meta
          name="description"
          content="اینجا می‌توانید کمی در مورد خودتون صحبت و توضیح دهید که محصول شما چیست و چه کارهایی انجام می‌ده"
        />
        <meta name="robots" content="index, follow" />
      </Head>
      <AboutUs />
      <Services />
      <VideoIntroduction />
      <Customers />
      <Cooperation />
      <Copyright />
    </>
  );
};

export default HomePage;
