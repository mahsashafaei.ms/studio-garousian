import React from 'react';
import styles from './Background.style';

interface Props {
  src: string;
}

const Background: React.FC<Props> = ({ children, src }) => {
  return (
    <div className="wrap backgroundDiv">
      <div className="itemsDiv">{children}</div>
      <img src={src} alt="background" className="background" />
      <style jsx>{styles}</style>
    </div>
  );
};

export default Background;
