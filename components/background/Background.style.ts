/* eslint-disable */
import css from 'styled-jsx/css';

export default css`
  .backgroundDiv {
    width: 100%;
    position: relative;
    overflow: hidden;
  }
  .itemsDiv {
    width: 100%;
    display: flex;
    align-items: center;
    flex-direction: row;
    position: absolute;
    z-index: 9999;
  }

  .wrap:after {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    background: rgba(34, 57, 155, 1);
    opacity: 80%;
    z-index: 999;
  }
  .background {
    min-width: 284%;
    margin-right: -104%;
    max-height: 30.3125rem;
    right: 0;
  }
  @media (min-width: 600px) {
    .background {
      max-width: 100%;
      min-height: 31.25rem;
      margin-right: -15%;
      min-width: 158%;
      max-height: 45.625rem;
    }
  }
  @media (min-width: 700px) {
    .background {
      max-height: 33.125rem;
      margin-right: -25%;
    }
  }
  @media (min-width: 1000px) {
    .background {
      min-width: 150%;
    }
  }
  @media (min-width: 1024px) {
    .background {
      width: 100%;
      height: 100%;
      margin: 0;
      max-height: 100%;
      min-width: 100%;
    }
  }
  @media (min-width: 1220px) {
    .background {
      min-width: 113%;
      margin-right: -10%;
      max-height: 47.5rem;
    }
  }
`;
