import React from 'react';
import styles from './Button.style';
import spinner from '../../assets/svg/spinner.svg';

interface Props {
  type: 'primary' | 'primaryFilled' | 'purpleNeutralFilled' | 'greenNeutralFilled' | 'orangeNeutralFilled';
  children: string;
  href?: string;
  className?: string;
  onClick?(): void;
  loading?: boolean;
}

const Button: React.FC<Props> = ({ type, children, href, className, onClick, loading }) => {
  return (
    <div className={`button ${className || ''}`}>
      <a
        href={href}
        role="button"
        className={type}
        onClick={() => {
          if (onClick) onClick();
        }}
      >
        {loading && <img className="loading" alt="loading" src={spinner} />}
        {children}
      </a>
      <style jsx>{styles}</style>
    </div>
  );
};

export default Button;
