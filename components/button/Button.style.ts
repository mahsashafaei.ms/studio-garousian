/* eslint-disable */
import css from 'styled-jsx/css';

export default css`
  .loading {
    width: 1.5rem;
    height: 1.5rem;
    margin-left: 0.5rem;
  }
  a.primaryFilled,
  a.primary {
    border-radius: 0.25rem;
    font-weight: 500;
    font-size: 1rem;
    padding: 0.625rem 0.875rem;
  }
  .primaryFilled {
    background: #5274f9;
    border: 1px solid #5274f9;
    color: #fff;
  }

  .primary {
    border: 1px solid #fff;
    color: #fff;
  }

  a.purpleNeutralFilled,
  a.greenNeutralFilled,
  a.orangeNeutralFilled {
    font-weight: 500;
    font-size: 0.875rem;
    padding: 0.375rem 0;
    display: block;
  }
  .purpleNeutralFilled {
    background: #edf1ff;
    color: #5c3ffb;
  }

  .greenNeutralFilled {
    background: #e9f9f0;
    color: #32ba6e;
  }

  .orangeNeutralFilled {
    background: #fef2eb;
    color: #fb7c3f;
  }
  @media (min-width: 767px) {
    a.primaryFilled,
    a.primary {
      font-size: 1.375rem;
      padding: 0.75rem 2rem;
    }
    a.purpleNeutralFilled,
    a.greenNeutralFilled,
    a.orangeNeutralFilled {
      font-size: 1rem;
    }
  }
`;
